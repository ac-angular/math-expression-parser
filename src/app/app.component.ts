import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @Input('vcmHematocrito') vcmHematocrito;
  @Input('vcmHematies') vcmHematies;
  @Input('hcmHemoglobina') hcmHemoglobina;
  @Input('hcmHematies') hcmHematies;
  @Input('ccmhHemoglobina') ccmhHemoglobina;
  @Input('ccmhHematocrito') ccmhHematocrito;

  vcm: number;
  formulas = {
    vcm: {
      description: 'Volumen Corpuscular Medio',
      formula: '[Hematocrito] * 100 / [Hematíes]'
    },
    hcm: {
      description: 'Hemoglobina Corpuscular Media',
      formula:'[Hemoglobina] * 100 / [Hematíes]'
    },
    ccmh: {
      description: 'Concentración Corpuscular Media de la Hemoglobina',
      formula: '[Hemoglobina] * 100 / [Hematocrito]'
    }
  };

  getVcm() {
    let formulaVcm = this.formulas.vcm.formula;
    formulaVcm = formulaVcm.replace('[Hematocrito]', this.vcmHematocrito + '');
    formulaVcm = formulaVcm.replace('[Hematíes]', this.vcmHematies + '');    
    this.vcm = eval(formulaVcm);
  }

  getHcm() {
    let formulaHcm = this.formulas.hcm.formula;
    formulaHcm = formulaHcm.replace('[Hemoglobina]', this.hcmHemoglobina + '');
    formulaHcm = formulaHcm.replace('[Hematíes]', this.hcmHematies + '');    
    this.vcm = eval(formulaHcm);
  }

  getCcmh() {
    let formulaCcmh = this.formulas.ccmh.formula;
    formulaCcmh = formulaCcmh.replace('[Hemoglobina]', this.ccmhHemoglobina + '');
    formulaCcmh = formulaCcmh.replace('[Hematocrito]', this.ccmhHematocrito + '');    
    this.vcm = eval(formulaCcmh);
  }

}
